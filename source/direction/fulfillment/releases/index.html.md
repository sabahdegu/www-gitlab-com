---
layout: markdown_page
title: "Fulfillment Releases"
description: This is the release page for Fulfillment.
canonical_path: "/direction/fulfillment/releases"
---

- TOC
{:toc}

# Past Releases

## Milestone 13.6

##### [Ability to purchase and automatically allocate additional storage](https://gitlab.com/groups/gitlab-org/-/epics/4663) `Sales` `Support` 
- Painpoint: Users are blocked by the 10GB per project storage limit with the only path to unblock being to reduce storage or stand-up a new project.
- Improvement: We are launching the ability to purchase additional storage and GitLab will automatically lift any repo storage limit blocks when additional storage is available
- Docs: We are still working on the formal docs for this feature, so please share our FAQ resources with customers in the meantime.
     1. https://about.gitlab.com/pricing/licensing-faq/#about-supplemental-storage
     1. https://about.gitlab.com/pricing/licensing-faq/#can-i-buy-more-storage

##### [Allow for user growth while an order is processed](https://gitlab.com/gitlab-org/license-gitlab-com/-/issues/184) [Self-managed] `Sales`
- Past behaviour: Some sales-assisted orders take a while to move from start to signed, by the time the order is signed and the license is sent, the instance has already outgrown the original seat count requested. This results in customers needing to place another order for additional seats.
- New behaviour: Now there is a 10% threshold for acceptable user growth during the processing period. The customer will be able to upload the license, even if their users have grown and the system will simply recognise the additional users as trueups to be reconciled at the next sales cycle.

##### [Free GitLab.com namespaces should display the number of seats currently in use](https://gitlab.com/gitlab-org/gitlab/-/issues/260378) `Sales` `Support`
- Painpoint: Customers (and sales) cannot easily identify how many seats a free namespace would be charged for if they enter into a subscription.
- Improvement: This change will list the current billable members count on the parent group's billing page for quick reference when making a "buy" decision.

##### [When pending members are approved by admins, log activity in Audit Events](https://gitlab.com/gitlab-org/gitlab/-/issues/276250) `Sales` `Support`
With the recent addition of [Users pending approval](https://docs.gitlab.com/ee/user/admin_area/approving_users.html), we have enabled admins to track who approves members via Audit Events.

##### [Display subscription name and end date for GitLab groups in admin](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/2163) `Support`
An awesome Support Team Contribution which now displays the subscription name and end date in the GitLab Groups page of CustomersDot admin.




## Milestone 13.5

##### [Manual EULA process is now deprecated](https://gitlab.com/groups/gitlab-org/-/epics/4606)  `Sales` `Support`
- Painpoint: Some customers needed to accept the End User License Agreement before a subscription was provisioned. This caused confusion, delays and inefficiency and the customer had to contact Sales or Support to get the product they had already paid for. 
- Improvement: Customers can now accept our terms when they upload their license to their instance. They longer no need to accept a EULA before the license gets generated. 

##### [Provide customers with tools to identify Billable Members](https://gitlab.com/groups/gitlab-org/-/epics/4547)  `Support` `Sales`
1. **Update Users API to include billable members** [Self Managed]
     - Past behavior: There was no easy way to get a list of the billable members on an instance. 
     - New behavior: Self-managed customers are able to get a list of the billable users on their instance. The list of users who take up a license seat should correspond with their billable users count on their Admin dashboard.
     - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/36750)
1. **API endpoint for billable members** [SAAS]
     - Past behavior: There was no easy way for a customer to get a list of their billable members on their gitlab.com group 
     - New behavior: Customers can now use the billable members API endpoint to get a list of billable members
     - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/217384), [Documentation](https://docs.gitlab.com/ee/api/members.html#list-all-billable-members-of-a-group)
1. **Static UI list of billable members** [SAAS]
     - Past behavior: There was no easy way for a customer to get a list of their billable members on their gitlab.com group.
     - New behavior: Customers can now look at their group's billing page for a list of users that take up a seat on their subscription.
     - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/216899)

##### [CI Minutes updates for Free tier subscribers](https://gitlab.com/groups/gitlab-org/-/epics/3510) `Sales` `Support` `Finance`
1. Updated monthly CI minutes allotment for free namespaces to 400
1. Updated the purchase price for additional CI minutes to $10/1000 minutes

##### [Original discounts are no longer available at renewal](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/790) `Sales` `Support` `Finance`
- Past behavior: When new customers purchase with a discount, the discounted pricing was locked inside the customer portal for renewals. Customers could proactively renew on customers portal with the same discount and this resulted in lost revenue. 
- New behavior: With this change, the original discounts are no longer available at renewal; customers will pay full price when renewing via customers.gitlab.com.

##### [All invoices are available in customers.gitlab.com](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/503) `Sales` `Support` `Finance`
- Past behavior: Customers had to request invoices from billing and sales as some invoices were not available to them. 
- New behavior: All invoices should now be available to download and view via customers.gitlab.com

##### [Self-managed Billable Users](https://gitlab.com/gitlab-org/gitlab/-/issues/233823) `Sales` `Support` `Finance`
In self-managed instances, we've changed labels to `Billable Users`where we are explicitly referring to users which are counted as taking up a license seat. This is to reduce confusion around inconsistency in using `Active users` within the admin pages.

##### [Notify admins as instance approaches user limit](https://gitlab.com/gitlab-org/gitlab/-/issues/230608#note_414081154) `Sales` `Support` 
As the self-managed instances approaches the subscribed user limit, we are now sending an email to all admins of the instance to prevent undesired surprise user overages.

##### [Project Bot no longer taking up seat](https://gitlab.com/gitlab-org/gitlab/-/issues/223695) `Sales` `Support`
The Access team recently released a new [Project Access Token feature](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#project-bot-users) which creates a GitLab-generated Bot users. This bot was taking up a license seat in the instance. We have now fixed this and moving forward, Project Bot will no longer take up a license seat. Unfortunately the Max User Count will not update for anyone who was affected by this.

##### [Minimum inactivity period for deactivation](https://gitlab.com/gitlab-org/gitlab/-/issues/201769) `Sales` `Support` 
We've updated the requirement from 180 days to 90 days which allows admin to deactivate users more timely. 

##### [Renewals booked at list](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/790) `Sales`
Renewals processed in customers.gitlab.com are now booking at list price. Prior to this change, the deals were closing at the discounted subscription amount if one was ever provided in the past.

##### Improved SaaS subscription internal administration tools `Sales` `Support` 
1. [Ability to search customers.gitlab.com admin by group name](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/273)
1. [Added "Max Seats Used" to GitLab Groups page on customers.gitlab.com admin](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/1910)



-----
# Upcoming Releases

## Milestone 13.7

##### [User cap to limit accidental user overage](https://gitlab.com/groups/gitlab-org/-/epics/4315) [Self-managed] `Sales` `Support`
- Past behaviour: Users were accidentally added to an instance, resulting in unforseen trueups and causing extra work for support and sales and a bad experience for the customer.
- New behaviour: Admins can now set a user cap on their instance. Once the instance has reached the cap, any users trying to join, will be placed in a pending approval state, where they do not count as a licensed seat yet, until they have been approved by the Admin.
- Docs: https://docs.gitlab.com/ee/user/admin_area/settings/sign_up_restrictions.html#user-cap 

##### [Automate Auto-Renewal process for billing team in CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/2081) `Finance`
This work will prevent the current manual processing on the part of the finance team which is estimated to save over 100 hours of administrative processing monthly.

##### [Incorrect Max and Users over license counts for non-12 month subscriptions](https://gitlab.com/gitlab-org/gitlab/-/issues/7008) `Sales` `Support`
- Pain Point: The current logic for Max users in self-managed instances doesn't work for subscriptions which are more or less than 12 months in duration. This causes issues for customers and sales in trying to determine if a user overage has occurred.
- Improvement: The Max user logic will be updated to account for the duration of the license term regardless of the actual length.
