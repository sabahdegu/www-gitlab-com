---
layout: handbook-page-toc
title: "OneTrust"
description: "OneTrust is privacy, security, and data governance software that marketing uses as our privacy and compliance solution on our websites."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Uses

OneTrust is privacy, security, and data governance software that marketing uses as our privacy and compliance solution on our websites. The marketing operations team works closely with our legal team and is primarily responsible for our privacy and compliance on our websites including cookie preferences. 

## Implementation

[See the epic](https://gitlab.com/groups/gitlab-com/-/epics/1265) for more information.
