---
layout: markdown_page
title: "TMRG - GitLab DiversABILITY"
description: "Helpful resources, support, and activities for differently abled team members or who are caring for a differently abled loved one."
canonical_path: "/company/culture/inclusion/erg-gitlab-diversability/"
---

## On this page
{:.no_toc}

- TOC
{:toc}



## Mission 

The GitLab DiversABILITY TMRG is a resource group to aid team members through their lived experiences at GitLab. Whether these experiences are seen or unseen, present themselves as a physical or a mental diversity, or directly impact you or a loved one, our goal is to intentionally build inclusive practices throughout the company as well as to act as a support system to team members who may want to be in a community with others who share similar experiences. 

The DiversABILITY TMRG advocates for every team member for physical conditions related to sight, sound, and/or mobility or for mental health concerns like ADHD, anxiety, depression and so much more. These are just a few examples of the lived experiences our team members could have at GitLab that can be better positioned with consideration and inclusion as part of the hiring, onboarding, and day-to-day processes as well as before, during, and after a project, a meeting, a conference, or even when we have big company-wide changes.

### How we accomplish our mission
First, we work with teams to intentionally consider how changes and experiences could impact team members, helping to ensure team members receive the support and tools necessary to be successful, whatever that may be. This helps to reduce the volume of "reactive" updates that need to be made. Think of our team experience at Contribute. We'll be planning ahead of our time together to make sure we're considering our lived experiences and that our onsite time at Contribute is as inclusive as possible.

Also, we act as a support group. Whether it's async in Slack or live during a meeting or panel conversation, we'll present members of our community, both from inside and outside of GitLab, to transparently discuss their experiences. Not just how they cope, but how they thrive.


## Leads
* [Melody Maradiaga](https://about.gitlab.com/company/team/#mmaradiaga) 
* Co-Lead [Wil Spillane](https://about.gitlab.com/company/team/#wspillane)


## Executive Sponsor
* [Dave Gilbert](https://about.gitlab.com/company/team/#davegilbert) - VP of Recruiting


## Upcoming Events




## Additional Resources
